#ifndef D_PINS_H
#define D_PINS_H

#include <avr/io.h>

#define LED1		5
#define LED1_DDR	DDRC
#define LED1_PORT	PORTC

#define LED2		4
#define LED2_DDR	DDRC
#define LED2_PORT	PORTC

#define LED1_AS_OUTPUT()	(LED1_DDR |= (1 << LED1))
#define LED2_AS_OUTPUT()	(LED2_DDR |= (1 << LED2))

#define LED1_OUT_LOW()		(LED1_PORT &= ~(1 << LED1))
#define LED2_OUT_LOW()		(LED2_PORT &= ~(1 << LED2))
#define LED1_OUT_HIGH()		(LED1_PORT |= (1 << LED1))
#define LED2_OUT_HIGH()		(LED2_PORT |= (1 << LED2))

#define LED1_TOGGLE()		(LED1_PORT & (1 << LED1) ? LED1_OUT_LOW() : LED1_OUT_HIGH())
#define LED2_TOGGLE()		(LED2_PORT & (1 << LED2) ? LED2_OUT_LOW() : LED2_OUT_HIGH())

#endif /* D_PINS_H */
