/* \file firmware.c							*/

#include <avr/interrupt.h>

#include "pins.h"
#include "uart.h"

#define UART_BAUD	38400

int main(void)
{
	LED1_AS_OUTPUT();
	LED2_AS_OUTPUT();

	LED1_OUT_HIGH();
	LED2_OUT_LOW();

	UART_Init(UART_BAUD);

	UCSRB |= (1 << RXCIE);

	sei();

	while(1)
	{
	}
}

ISR(USART_RXC_vect)
{
	uint8_t c;

	c = UDR;

	LED1_TOGGLE();
}
