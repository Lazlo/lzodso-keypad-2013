/*! \file common.h */

#pragma once

#define ARSIZE(a) (sizeof(a) / sizeof(a[0]))
