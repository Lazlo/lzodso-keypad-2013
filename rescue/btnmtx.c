/*! \file btnmtx.c
 *! \brief button matrix library */

#include "config.h"
#include "uart.h"
#include "strutils.h"
#include "btnmtx.h"

#include <util/delay.h>

#define CONFIG_BTNMTX_KEYQUESIZE 8

struct btnmtx_key {
	uint8_t key;
	uint8_t direction;
};

struct btnmtx_keyque {
	uint8_t head;
	uint8_t tail;
	uint8_t size;
	struct btnmtx_key *buf;
};

static BtnMtx_t *g_bm;
static struct btnmtx_keyque g_bm_keyque;
static struct btnmtx_key g_bm_keybuf[CONFIG_BTNMTX_KEYQUESIZE];

static void BtnMtxIO_Init(BtnMtx_t *bm);
static void BtnMtx_InitKeyQue(struct btnmtx_keyque *q, struct btnmtx_key *buf, const uint8_t bufsize);
static void BtnMtx_EnqueueKey(struct btnmtx_keyque *q, const uint8_t key, const uint8_t direction);
static void BtnMtx_DequeueKey(struct btnmtx_keyque *q, uint8_t *key, uint8_t *direction);
static void BtnMtx_SetColData(const uint8_t col, const uint8_t data);
static uint8_t BtnMtx_ColDataChanged(const uint8_t col);
static uint8_t BtnMtx_GetKeyChanged(const uint8_t col, uint8_t *key, uint8_t *direction);
static void BtnMtx_Scan(void);

static void BtnMtxIO_Init(BtnMtx_t *bm)
{
	uint8_t i;
	uint8_t v;

	for (i = 0; i < bm->nfo->ncols; i++)
	{
		v = bm->nfo->keys_activelow ? 0xff : 0;
		bm->keys_last[i] = v;
		bm->keys[i] = v;
	}
}

static void BtnMtx_InitKeyQue(struct btnmtx_keyque *q, struct btnmtx_key *buf, const uint8_t bufsize)
{
	uint8_t i;

	q->head = 0;
	q->tail = 0;
	q->size = bufsize;
	q->buf = buf;

	for (i = 0; i < q->size; ++i) {
		q->buf[i].key = 0;
		q->buf[i].direction = 0;
	}
}

static void BtnMtx_EnqueueKey(struct btnmtx_keyque *q, const uint8_t key, const uint8_t direction)
{
	if (q->tail < q->size) {
		q->buf[q->tail].key = key;
		q->buf[q->tail].direction = direction;

		++q->tail;
	}
}

static void BtnMtx_DequeueKey(struct btnmtx_keyque *q, uint8_t *key, uint8_t *direction)
{
	if (q->head < q->tail) {
		*key = q->buf[q->head].key;
		*direction = q->buf[q->head].direction;

		++q->head;
	} else {
		q->head = 0;
		q->tail = 0;
	}
}

/*! \brief Initialize button matrix library */
void BtnMtx_Init(BtnMtx_t *bm)
{
	g_bm = bm;

	BtnMtxIO_Init(g_bm);
	BtnMtx_InitKeyQue(&g_bm_keyque, g_bm_keybuf, CONFIG_BTNMTX_KEYQUESIZE);
}

/*! Insert column data into the logic of this library.
 *! \param col Numeric identifier for the column the data belongs to
 *! \param data Column data to feed into the library. */
static void BtnMtx_SetColData(const uint8_t col, const uint8_t data)
{
	if (col >= g_bm->nfo->ncols)
		return;

	g_bm->keys[col] = g_bm->nfo->keys_activelow ? ~data : data;
}

/*! Check if last and current column data differ. */
static uint8_t BtnMtx_ColDataChanged(const uint8_t col)
{
	return (g_bm->keys_last[col] != g_bm->keys[col]);
}

/*! Identify the key that changed and the kind of change in a column.
 *! \param col Numeric identifier for the column
 *! \param key Numeric identifier for the key that changed
 *! \param direction Identifies the kind of change
 *! \return number of keys that changed in the column */
static uint8_t BtnMtx_GetKeyChanged(const uint8_t col, uint8_t *key, uint8_t *direction)
{
	uint8_t nchanged;
	uint8_t b;
	uint8_t last;
	uint8_t curr;

	nchanged = 0;

	/* Identify the key (bit) that changed */
	for (b = 0; b < 8; b++)
	{
		last = g_bm->keys_last[col] & (1 << (7 - b));
		curr = g_bm->keys[col] & (1 << (7 - b));

		if (last != curr)
		{
			/* Calculate and set the key id */
			*key = (1 + col) + (g_bm->nfo->ncols * b);

			*direction = (last < curr) ? 1 : 0; /* 1=up,0=down */

			nchanged++;
		}
	}

	if (nchanged > 0)
	{
		g_bm->keys_last[col] = g_bm->keys[col];
	}

	return nchanged;
}

#define bmout_putc	UART_Pucc
#define bmout_puts	UART_Puts
#define bmdbg_putc	UART_Putc
#define bmdbg_puts	UART_Puts

/*! Scan the complete button matrix once */
static void BtnMtx_Scan(void)
{
	uint8_t i;
	uint8_t rxb;
#ifdef CONFIG_DEBUG_SHR_IO
	char rxb_str[8+1];
#endif
	uint8_t key;
	uint8_t direction;
#ifdef CONFIG_DEBUG_BTNMTX
	char key_str[2+1];
#endif

	g_bm->ops->reset();
	for (i = 0; i < 5; i++) {
		/* Select a column in the button matrix */
		g_bm->ops->write((1 << (7-i)));
		/* Latch the outputs of the matrix rows for the current column */
		g_bm->ops->load();
		/* Read the state of the keys from all rows for the current column */
		rxb = g_bm->ops->read();
#ifdef CONFIG_DEBUG_SHR_IO
		itob(rxb, rxb_str);
		bmdbg_puts("btnmtx ");
		bmdbg_puts("o addr ");
		bmdbg_putc('0'+7-i);
		bmdbg_puts(", ");
		bmdbg_puts("i data ");
		bmdbg_puts(rxb_str);
#endif
		/* Feed the data for the current column into the button matrix logic module */
		BtnMtx_SetColData(i, rxb);
		/* Check if a key changed */
		if (BtnMtx_ColDataChanged(i)) {
			/* Get the key id of the key that changed */
			BtnMtx_GetKeyChanged(i, &key, &direction);
			/* Append key to end of key queue */
			BtnMtx_EnqueueKey(&g_bm_keyque, key, direction);

#ifdef CONFIG_DEBUG_BTNMTX
			itoa(key, key_str, 10);
#ifdef CONFIG_DEBUG_SHR_IO
			bmdbg_puts(", ");
#endif
			bmdbg_puts("key ");
			bmdbg_puts(key_str);
			bmdbg_puts((direction ? " up" : " down"));
#if !defined(CONFIG_DEBUG_SHR_IO)
			bmdbg_puts("\r\n");
#endif
#endif
		}
#if defined(CONFIG_DEBUG_SHR_IO) || defined(CONFIG_DEBUG_SHR_IO_SLOW)
		bmdbg_puts("\r\n");
#endif
#ifdef CONFIG_DEBUG_SHR_IO_SLOW
		_delay_ms(1000);
#endif
		}
#if defined(CONFIG_DEBUG_SHR_IO) || defined(CONFIG_DEBUG_SHR_IO_SLOW)
	bmdbg_puts("btnmtx fin\r\n");
#endif
#ifdef CONFIG_DEBUG_SHR_SCAN
	_delay_ms(1000);
#endif
}

void BtnMtx_Update(void)
{
	BtnMtx_Scan();
}

void BtnMtx_GetKey(uint8_t *key_id, uint8_t *direction)
{
	BtnMtx_DequeueKey(&g_bm_keyque, key_id, direction);
}
