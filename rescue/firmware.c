/*! \file firmware.c
 *! \brief Keypad controller firmware
 *!
 *! This firmware will scan the connected keypads button matrix
 *! and rotary encoders, detect keys that have been pressed or
 *! released and pass that information on via the UART.
 *!
 *! Firmware written for the ATmega8 running at 16 MHz
 *! The fuse bits are set to:
 *! - efuse: FF
 *! - lfuse: DF
 *! - hfuse: D9 */

#include "config.h"
#include "uart.h"
#include "strutils.h"
#include "siposhr.h"
#include "pisoshr.h"
#include "kbd.h"

int main(void)
{
	/* Button Matrix Configuration */
	struct btnmtx_nfo bm_nfo = {
		.ncols			= 5,
		.keys_activelow		= 1,
	};
	/* Rotary Encoder Configurations */
	struct rotenc_nfo re_nfo[] = {
		/* 7 x Noble RE0124 */
		/* CW	3 1 0 2 */
		/* CCW	3 2 0 1 */
		[0] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[1] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[2] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[3] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[4] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[5] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		[6] = { .tabs = 24, .bpt = 4, .ngraybits = 2 },
		/* 1 x Panasonic EVEQ */
		/* CW	3 1 0 2 */
		/* CCW	3 2 0 1 */
		[7] = { .tabs = 32, .bpt = 2, .ngraybits = 2 },
	};
	/* Button Matrix IO Operations */
	struct btnmtx_scan_ioops bm_ioops = {
		.reset	= SIPOSHR_Reset,
		.write	= SIPOSHR_Write,
		.load	= PISOSHR_Load,
		.read	= PISOSHR_Read,
	};
	/* Rotary Encoder IO Operations */
	struct rotenc_read_ioops re_ioops = {
		.read	= PISOSHR_Read,
	};

	/* Button Matrix object */
	struct btnmtx bm = {
		.ops			= &bm_ioops,
		.nfo			= &bm_nfo,
	};
	/* Rotary Encoder Set object */
	struct rotenc re = {
		.ops			= &re_ioops,
		.nfo			= re_nfo,
	};
	/* Keyboard object */
	struct kbd kbd = {
		.bm			= &bm,
		.re			= &re,
	};

	UART_Init(USART_BAUD);
	UART_Puts("UART ready!\r\n");

	/* Initialize access to shift registers */
	SIPOSHR_Init();
	PISOSHR_Init();

	Kbd_Init(&kbd);
	UART_Puts("Kbd ready!\r\n");
	while (1) {
		Kbd_Update();
	}
	return 0;
}
