/*! \file rotenc.c */

#include "config.h"
#include "common.h"
#include "uart.h"
#include "strutils.h"
#include "rotenc.h"

#define CONFIG_ROTENC_RXBSIZE 2

#define CONFIG_ROTENC_KEYBUFSIZE 8

#define reout_putc	UART_Putc
#define reout_puts	UART_Puts

#if defined(CONFIG_DEBUG_ROTENC) || \
	defined(CONFIG_DEBUG_ROTENC_IO) || \
	defined(CONFIG_DEBUG_ROTENC_BITCHANGE) || \
	defined(CONFIG_DEBUG_ROTENC_DECODE)

#define redbg_putc	UART_Putc
#define redbg_puts	UART_Puts

#else

#define redbg_putc(c)
#define redbg_puts(s)

#endif

#ifdef CONFIG_DEBUG_ROTENC
#define redbg_p_re(re_id, direction)		\
	do {					\
		redbg_puts("rotenc ");		\
		redbg_putc('0' + re_id);	\
		redbg_putc(' ');		\
		switch (direction) {		\
		case 1:				\
			redbg_puts("cw");	\
			break;			\
		case 0:				\
			redbg_puts("ccw");	\
			break;			\
		}				\
		redbg_puts("\r\n");		\
	} while (0);
#else
#define redbg_p_re(re_id, direction)
#endif

#ifdef CONFIG_DEBUG_ROTENC_BITCHANGE
#define redbg_p_re_changed(re_id, byte, bit)	\
	do {					\
		redbg_puts("rotenc");		\
		redbg_putc(' ');		\
		redbg_putc('0' + re_id);	\
		redbg_putc(' ');		\
		redbg_puts("byte ");		\
		redbg_putc('0' + byte);		\
		redbg_putc(' ');		\
		redbg_puts("bit ");		\
		redbg_putc('0' + bit);		\
		redbg_puts("\r\n");		\
	} while (0);
#else
#define redbg_p_re_changed(re_id, byte, bit)
#endif

#ifdef CONFIG_DEBUG_ROTENC_IO
#define redbg_p_io_read(io)			\
	do {					\
		char tmp_str[9];		\
		redbg_puts("rotenc i data ");	\
		itob(io->rxb[1], tmp_str);	\
		redbg_puts(tmp_str);		\
		redbg_puts(" ");		\
		itob(io->rxb[0], tmp_str);	\
		redbg_puts(tmp_str);		\
		redbg_puts("\r\n");		\
	} while (0);
#else
#define redbg_p_io_read(io)
#endif

#ifdef CONFIG_DEBUG_ROTENC_DECODE
#define redbg_p_dir(re_id, byte, offset, mask, last, curr)	\
	do {					\
		char tmpstr[9];			\
		itoa(mask, tmpstr, 2);		\
		redbg_puts("rotenc ");		\
		redbg_putc('0'+re_id);		\
		redbg_putc(' ');		\
		redbg_puts("byte ");		\
		redbg_putc('0'+byte);		\
		redbg_putc(' ');		\
		redbg_puts("offset ");		\
		redbg_putc('0'+offset);		\
		redbg_putc(' ');		\
		redbg_puts("mask ");		\
		redbg_puts(tmpstr);		\
		redbg_putc(' ');		\
		redbg_puts("last ");		\
		redbg_putc('0'+last);		\
		redbg_putc(' ');		\
		redbg_puts("curr ");		\
		redbg_putc('0'+curr);		\
		redbg_puts("\r\n");		\
	} while (0);
#else
#define redbg_p_dir(re_id, byte, offset, mask, last, curr)
#endif

struct rotenc_io {
	uint8_t init;
	uint8_t rxbsize;
	uint8_t *rxb_last;
	uint8_t *rxb;
	uint8_t (*read)(void);
};

struct rotenc_key {
	uint8_t re_id;
	uint8_t direction;
};

struct rotenc_keyque {
	uint8_t head;
	uint8_t tail;
	uint8_t size;
	struct rotenc_key *buf;
};

static struct rotenc_io g_re_io;
static struct rotenc_keyque g_re_keyque;
static struct rotenc *g_re;
static uint8_t g_re_rxb[CONFIG_ROTENC_RXBSIZE];
static uint8_t g_re_rxb_last[CONFIG_ROTENC_RXBSIZE];
static struct rotenc_key g_re_keybuf[CONFIG_ROTENC_KEYBUFSIZE];

static void RotEnc_ReadAll(struct rotenc_io *io);
static void RotEnc_SaveCurrAsLast(struct rotenc_io *io);
static uint8_t RotEnc_AnyChanged(struct rotenc_io *io);
static uint8_t RotEnc_GetKeyChanged(struct rotenc_io *io);
static uint8_t RotEnc_GetDirection(struct rotenc_io *io, const uint8_t re_id);
static void RotEnc_InitKeyQue(struct rotenc_keyque *q, struct rotenc_key *buf, const uint8_t bufsize);
static void RotEnc_EnqueueKey(struct rotenc_keyque *q, const uint8_t re_id, const uint8_t direction);
static void RotEnc_DequeueKey(struct rotenc_keyque *q, uint8_t *re_id, uint8_t *direction);

/* Perform the actual read operation and save the current state of all rotary
 * encoders.
 * Note that the bytes read are stored in reverse order! */
static void RotEnc_ReadAll(struct rotenc_io *io)
{
	uint8_t n = io->rxbsize;
	uint8_t i;
	for (i = 0; i < n; ++i)
		io->rxb[n-1-i] = (io->read)();
}

static void RotEnc_SaveCurrAsLast(struct rotenc_io *io)
{
	uint8_t n = io->rxbsize;
	uint8_t i;
	for (i = 0; i < n; ++i)
		io->rxb_last[i] = io->rxb[i];
}

/* Check if the state just read differs from one before */
static uint8_t RotEnc_AnyChanged(struct rotenc_io *io)
{
	uint8_t n = io->rxbsize;
	uint8_t i;
	for (i = 0; i < n; ++i)
		if (io->rxb[i] != io->rxb_last[i])
			return 1;
	return 0;
}

/* Identify the rotary encoder that changed */
static uint8_t RotEnc_GetKeyChanged(struct rotenc_io *io)
{
	uint8_t n = io->rxbsize;
	uint8_t i;
	uint8_t bit;
	uint8_t last;
	uint8_t curr;
	uint8_t re_id = 0;

	for (i = 0; i < n; ++i) {
		curr = io->rxb[i];
		last = io->rxb_last[i];
		if (curr == last)
			continue;
		for (bit = 0; bit < 8; ++bit) {
			if ((curr & (1 << bit)) != (last & (1 << bit))) {

				re_id = 1 + (((i * 8) + bit) / 2);

				redbg_p_re_changed(re_id, i, bit);
			}
		}
	}
	return re_id;
}

static uint8_t RotEnc_GetDirection(struct rotenc_io *io, const uint8_t re_id)
{
	struct rotenc_nfo *re_nfo;
	uint8_t i;
	uint8_t offset;
	uint8_t mask;
	uint8_t last;
	uint8_t curr;
	uint8_t direction;

	/* Get pointer to rotenc_nfo struct for the specified rotary encoder */
	re_nfo = &g_re->nfo[re_id-1];

	/* Calculate the subscript to rxb
	 * Take re_id and subtract one
	 * Divide by 4 (2 bits per encoder, so 4 fit in a uint8_t byte) */
	i = (re_id - 1) / 4;

	/* Calculate the distance in bits from LSB (right) to the position
	 * just before the graybits begin in the rxb byte
	 *
	 * - Take modulo 4 of re_id - 1
	 * - Multiply by 2 (number of graybits, while assuming all other
	 *   encoders also have 2 graybits which is very bad, unless all
	 *   the encoders connected have the same number of graybits */
	offset = (((re_id - 1) % 4) * 2);

	/* Generate a bitmask to read only the bits for this encoder
	 *
	 * - Create a bitmask with respect to the number of graybits
	 *   while the mask starts at the LSB
	 * - Then shift the mask right in order to match the position
	 *   of the bits in the rxb byte */
	mask = ((1 << re_nfo->ngraybits) - 1) << offset;

	/* Get the current and last bits
	 * Lookup the byte, mask off the bits and shift them towards the
	 * LSB (bit 0) */
	last = (io->rxb_last[i] & mask) >> offset;
	curr = (io->rxb[i] & mask) >> offset;

	redbg_p_dir(re_id, i, offset, mask, last, curr);

	/* Process current and last bits to identify direction */

	if ((last == 3 && curr == 1) ||
			(last == 1 && curr == 0) ||
			(last == 0 && curr == 2) ||
			(last == 2 && curr == 3)) {
		direction = 1;
	}
	if ((last == 3 && curr == 2) ||
		(last == 2 && curr == 0) ||
		(last == 0 && curr == 1) ||
		(last == 1 && curr == 3)) {
		direction = 0;
	}
	return direction;
}

/* Initialize key que */
static void RotEnc_InitKeyQue(struct rotenc_keyque *q, struct rotenc_key *buf, const uint8_t bufsize)
{
	uint8_t i;

	q->head = 0;
	q->tail = 0;
	q->size = bufsize;
	q->buf = buf;

	for (i = 0; i < q->size; ++i) {
		q->buf[i].re_id = 0;
		q->buf[i].direction = 0;
	}
}

/* Append key to tail of key queue */
static void RotEnc_EnqueueKey(struct rotenc_keyque *q, const uint8_t re_id, const uint8_t direction)
{
	if (q->tail < q->size) {
		q->buf[q->tail].re_id = re_id;
		q->buf[q->tail].direction = direction;

		++q->tail;
	}
}

/* Remove key from head of queue */
static void RotEnc_DequeueKey(struct rotenc_keyque *q, uint8_t *re_id, uint8_t *direction)
{
	if (q->head < q->tail) {
		*re_id = q->buf[q->head].re_id;
		*direction = q->buf[q->head].direction;

		++q->head;
	} else {
		q->head = 0;
		q->tail = 0;
	}
}

/* Initialize the rotary encoder library */
void RotEnc_Init(struct rotenc *re)
{
	struct rotenc_io *io = &g_re_io;

	g_re = re;

	io->init = 0;
	io->rxbsize = CONFIG_ROTENC_RXBSIZE;
	io->rxb = g_re_rxb;
	io->rxb_last = g_re_rxb_last;
	io->read = g_re->ops->read;

	RotEnc_InitKeyQue(&g_re_keyque, g_re_keybuf, CONFIG_ROTENC_KEYBUFSIZE);
}

/* Let the library process the current state of all rotary encoders */
void RotEnc_Update(void)
{
	struct rotenc_io *io = &g_re_io;
	uint8_t re_id;
	uint8_t direction;
	static uint8_t re_id_last;
	static uint8_t direction_last;
	static uint8_t cnt;

	RotEnc_ReadAll(io);
	redbg_p_io_read(io);

	if (!io->init) {
		reout_puts("rotenc init\r\n");
		RotEnc_SaveCurrAsLast(io);
		io->init = 1;
	}
	if (!RotEnc_AnyChanged(io))
		return;
	re_id = RotEnc_GetKeyChanged(io);
	direction = RotEnc_GetDirection(io, re_id);

	if (re_id == re_id_last && direction == direction_last) {
		if (++cnt == g_re->nfo[re_id-1].bpt) {
			redbg_p_re(re_id, direction);
			RotEnc_EnqueueKey(&g_re_keyque, re_id, direction);
			cnt = 0;
		}
	} else {
		re_id_last = re_id;
		direction_last = direction;
		cnt = 1;
	}

	RotEnc_SaveCurrAsLast(io);
}

void RotEnc_GetKey(uint8_t *re_id, uint8_t *direction)
{
	RotEnc_DequeueKey(&g_re_keyque, re_id, direction);
}
