/*! \file config.h */

#pragma once

#define USART_BAUD 38400

//#define CONFIG_DEBUG_BTNMTX

#define CONFIG_ROTENC

//#define CONFIG_DEBUG_ROTENC

//#define CONFIG_DEBUG_ROTENC_BITCHANGE

//#define CONFIG_DEBUG_ROTENC_IO

//#define CONFIG_DEBUG_ROTENC_DECODE

//#define CONFIG_DEBUG_SHR_IO

/* Set the value of this macro to 1 to pause for 1 second after a scan
 * (via shift-registers) is complete (to allow to observe the data
 * resulting from the scan. */
//#define CONFIG_DEBUG_SHR_SCAN

/* Define this macro to pause for 1 second after every single write/read-pair
 * to the shift-registers. This is useful when you need to verify the correct
 * operation of the outputs of the 74HC164 that are used as address lines to
 * select/activate a column in the button matrix. */
//#define CONFIG_DEBUG_SHR_IO_SLOW
