/*! \file rotenc.h */

#pragma once

#include <stdint.h>

struct rotenc_read_ioops {
	uint8_t (*read)(void);
};

struct rotenc_nfo {
	uint8_t tabs; /* tabs per 360° shaft rotation */
	uint8_t bpt; /* bits per tab that change */
	uint8_t ngraybits;
};

struct rotenc {
	struct rotenc_nfo *nfo;
	struct rotenc_read_ioops *ops;
};

void RotEnc_Init(struct rotenc *re);
void RotEnc_Update(void);
void RotEnc_GetKey(uint8_t *re_id, uint8_t *direction);
