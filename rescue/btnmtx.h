/*! \file btnmtx.h
 *! \brief button matrix library */

#pragma once

#include <inttypes.h>

#define BTNMTX_COLS 5

struct btnmtx_scan_ioops {
	void (*reset)(void);
	void (*write)(uint8_t);
	void (*load)(void);
	uint8_t (*read)(void);
};

struct btnmtx_nfo {
	uint8_t ncols;
	uint8_t keys_activelow;
};

struct btnmtx {
	struct btnmtx_nfo *nfo;
	struct btnmtx_scan_ioops *ops;
	uint8_t keys_last[BTNMTX_COLS];
	uint8_t keys[BTNMTX_COLS];
};

typedef struct btnmtx BtnMtx_t;

void BtnMtx_Init(BtnMtx_t *bm);
void BtnMtx_Update(void);
void BtnMtx_GetKey(uint8_t *key, uint8_t *direction);
