/*! \file kbd.h */

#pragma once

#include "btnmtx.h"
#include "rotenc.h"

struct kbd {
	struct btnmtx *bm;
	struct rotenc *re;
};

void Kbd_Init(struct kbd *kbd);
void Kbd_Update(void);
