/*! \file kbd.c */

#include "config.h"
#include "btnmtx.h"
#include "rotenc.h"
#include "kbd.h"

#include <util/delay.h>

#include "uart.h"
#include "strutils.h"

#define kbdbg_putc	UART_Putc
#define kbdbg_puts	UART_Puts

#define kbdbg_p_key(key_id, direction)		\
	do {					\
		char tmpstr[4];			\
		itoa(key_id, tmpstr, 10);	\
		kbdbg_puts("kbd: key ");	\
		kbdbg_puts(tmpstr);		\
		kbdbg_putc(' ');		\
		if (direction)			\
			kbdbg_puts("up");	\
		else				\
			kbdbg_puts("down");	\
		kbdbg_puts("\r\n");		\
	} while (0);

#define kbdbg_p_re(re_id, direction)		\
	do {					\
		kbdbg_puts("kbd: re ");		\
		kbdbg_putc('0'+re_id);		\
		kbdbg_putc(' ');		\
		if (direction)			\
			kbdbg_puts("cw");	\
		else				\
			kbdbg_puts("ccw");	\
		kbdbg_puts("\r\n");		\
	} while (0);

static struct kbd *k;

void Kbd_Init(struct kbd *kbd)
{
	k = kbd;

	/* Initialize logic */
	BtnMtx_Init(kbd->bm);
	RotEnc_Init(kbd->re);
}

void Kbd_Update(void)
{
	uint8_t key;
	uint8_t direction;

	key = direction = 0;
	BtnMtx_Update();
	BtnMtx_GetKey(&key, &direction);
	if (key) {
		kbdbg_p_key(key, direction);
	}

#ifdef CONFIG_ROTENC
	key = direction = 0;
	RotEnc_Update();
	RotEnc_GetKey(&key, &direction);
	if (key) {
		kbdbg_p_re(key, direction);
	}
#ifdef CONFIG_DEBUG_ROTENC
	_delay_ms(100);
#endif
#endif
}
