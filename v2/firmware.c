/* \file firmware.c							*/

#include "config.h"
#include "pins.h"
#include "sch.h"
#include "uart.h"

static void LED_Update(void)
{
	LED1_TOGGLE();
}

int main(void)
{
	LED1_AS_OUTPUT();
	LED2_AS_OUTPUT();

	LED1_OUT_HIGH();
	LED2_OUT_LOW();

	Sch_Init(SCH_TICK_MS);

	Sch_Add_Task(LED_Update, 0, 500);

	Sch_Start();

	while(1)
	{
		Sch_Dispatch_Tasks();
	}
}
