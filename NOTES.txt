LZODSO KEYPAD FIRMWARE

== INIT ==
- setup USART (for debugging)
- setup timer for scheduler
- setup scheduler
 - add keypad task (init, IO, process, busifresponder)
- start scheduler
 - enable interrupts (timer)
== LOOP ==
- check if initialized, if so make KCINT output high, else run initialization
 - setup GPIOs for keypad (make KCINT output low)
 - setup keypad key buffer (queue)
 - run initial scan (to be aware of the current state of rotary encoders and buttons)
 - setup bus interface (I2C or SPI)
  - listen for requests from bus interface
   - register keypad code to respond to busif specific "request from master"-interrupt
   - enable busif-specific interrupt
 - set KCINT high when initialization is finished
- scan keypad
 - do IO (select column, read row)
 - process data read (compare with last state)
  - if a key change is detected
   - add entries for changed keys in key buffer
   - signal "key changed" interrupt (pull KCINT low)
 - check if requested by bus master (flag is set from interrupt service routine)
  - dequeue key buffer entries after they have been read by the bus master
  - make KCINT high again when request has been served
- sleep/idle
