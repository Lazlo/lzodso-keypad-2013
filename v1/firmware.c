//! \file firmware.c
//! \brief Keypad controller firmware
//!
//! This firmware will scan the connected keypads button matrix
//! and rotary encoders, detect keys that have been pressed or
//! released and pass that information on via the UART.
//!
//! Firmware written for the ATmega8 running at 16 MHz
//! The fuse bits are set to:
//! - lfuse: DF
//! - hfuse: D9

//--- Configuration macro constants -------------------------------------------

//! Periode in miliseconds for the scheduler to run.
#define SCH_TICK_MS 1

//! Baud rate to use for the UART
#define UART_BAUD 38400

//! Enable use of TWI interface by defining CONFIG_TWI
//#define CONFIG_TWI

//! TWI slave address (only used when CONFIG_TWI is also defined).
#define TWI_SLA 0x25

//--- Includes ----------------------------------------------------------------

#include "pins.h"
#include "uart.h"
#include "pc_0.h"
#include "kbd.h"
#include "kci.h"
#include "sch.h"
#include "strutils.h"
#ifdef CONFIG_TWI
#include "twi.h"
#endif /* CONFIG_TWI */

//--- Private function prototypes ---------------------------------------------

static void Write_KbdKey_To_Link(void);
#ifdef CONFIG_TWI
static void Dispatch_TWI(void);
#endif /* CONFIG_TWI */

//--- Private variables -------------------------------------------------------

//--- Public functions --------------------------------------------------------

//! \brief Point of entry for firmware execution.
//!
//! WHY DOES THIS NOT SHOW IN DOXYGEN?!
int main(void)
{
	// Setup Key-Change Interrupt port pin
	KCI_AS_OUTPUT();
	KCI_OUT_LOW();

	// Initialize scheduler
	Sch_Init(SCH_TICK_MS);

	// Initialize the UART for debugging
	UART_Init(UART_BAUD);

	UART_Puts("\r\n");
	UART_Puts("uart ready!\r\n");

	// Initialize the communication buffer module (used in combination
	// with the UART).
	link_Init(UART_Putc);

#ifdef CONFIG_TWI
	// Initialize bus interface (TWI)
	TWI_Init(TWI_SLA, 0);
#endif /* CONFIG_TWI */

	// Initialize keyboard
	Kbd_Init();

	// Add tasks to scheduler
	Sch_Add_Task(link_Update, 0, 1);
	Sch_Add_Task(Write_KbdKey_To_Link, 3, 50);
#ifdef CONFIG_TWI
	Sch_Add_Task(Dispatch_TWI, 7, 1);
#endif /* CONFIG_TWI */

	// Start the scheduler
	Sch_Start();

	// Loop forever
	while(1)
	{
		// Let the scheduler do its work (execute tasks).
		Sch_Dispatch_Tasks();
	}
}

//--- Private functions -------------------------------------------------------

static void Write_KbdKey_To_Link(void)
{
	uint8_t key = 0;
	char key_str[3];

	Kbd_Update();

	if(Kbd_GetKey(&key) == 0)
		return;

	itoa(key, key_str, 10);

	link_Puts(key_str);
	link_Puts("\r\n");
}

#ifdef CONFIG_TWI
static void Dispatch_TWI(void)
{
	uint8_t status;
	char status_str[3];
	uint8_t ack;

	// nothing changed
	if(!TWI_StatusChanged())
		return;

	//
	// Slave Transmitter
	//

	// Either ...
	status = TWI_GetStatus();
	switch(status)
	{

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Status codes
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// "Bus error due to an illegal START or STOP condition"
	case 0x00:
	break;

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Status codes for TWI Slave Transmitter Mode
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// "Own SLA+R has been received; ACK has been returned" (0xA8)
	case 0xA8:
		link_Puts("RX SLA+R\r\n");

		// transmit the bytes from the key buffer
		link_Puts("TX 0xBE");
		ack = 0;
		status = TWI_Write(0xBE, ack);
		if(status)
		{
			// Failed
			link_Puts(" failed (");

			if(status == 1)
			{
				link_Puts(" timeout");
			}
			else
			{
				itoa(status, status_str, 16);

				link_Puts("0x");
				link_Puts(status_str);
			}
			link_Puts(")!\r\n");

			return;
		}
		link_Puts(", RX ");
		if(ack == 0)
			link_Putc('N');
		link_Puts("ACK");
		link_Puts("\r\n");
	break;

	// "Arbitration lost in SLA+R/W as Master; own SLA+R has been received; ACK has been returned" (0xB0)
	case 0xB0:
		link_Puts("arb lost\r\n");
	break;

	// "Data byte in TWDR has been transmitted; ACK has been received" (0xB8)
	case 0xB8:
		link_Puts("TX d, ack\r\n");
	break;

	// "Data byte in TWDR has been transmitted; NOT ACK has been received" (0xC0)
	case 0xC0:
		link_Puts("TX d, Nack\r\n");
	break;

	// "Last data byte in TWDR has been transmitted; ACK has been received" (0xC8)
	case 0xC8:
		link_Puts("TX ld, ack\r\n");

		TWI_ST_Reset();
	break;

	default:
		itoa(status, status_str, 16);

		link_Puts("ST 0x");
		link_Puts(status_str);
		link_Puts("\r\n");
	break;
	}
}
#endif /* CONFIG_TWI */
