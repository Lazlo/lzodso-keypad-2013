//! \file
//! \brief Port pin configuration

#ifndef D_PINS_H
#define D_PINS_H

#include <avr/io.h>

//
// UART
//

// Port pins PD1 and PD0 are used by the UART interface

// PD1	TXD
// PD0	RXD

//
// SIPO (see siposhr.c)
//

//! 74HC164 clock port pin offset
#define SIPOSHR_CP		5
//! 74HC164 clock port pin data direction register
#define SIPOSHR_CP_DDR		DDRD
//! 74HC164 clock port pin data output register
#define SIPOSHR_CP_PORT		PORTD

//! 74HC164 data port pin offset
#define SIPOSHR_DS1		7
//! 74HC164 data port pin data direction register
#define SIPOSHR_DS1_DDR		DDRD
//! 74HC164 data port pin data output register
#define SIPOSHR_DS1_PORT	PORTD

//! 74HC164 reset port pin offset
#define SIPOSHR_MR		6
//! 74HC164 reset port pin data direction register
#define SIPOSHR_MR_DDR		DDRD
//! 74HC164 reset port pin data output register
#define SIPOSHR_MR_PORT		PORTD

//
// PISO (see pisoshr.c)
//

//! 74HC165 clock port pin offset
#define PISOSHR_CLK		0
//! 74HC165 clock port pin data direction register
#define PISOSHR_CLK_DDR		DDRC
//! 74HC165 clock port pin data output register
#define PISOSHR_CLK_PORT	PORTC

//! 74HC165 data port pin offset
#define PISOSHR_Q		2
//! 74HC165 data port pin data direction register
#define PISOSHR_Q_DDR		DDRC
//! 74HC165 data port pin data input register
#define PISOSHR_Q_PIN		PINC

//! 74HC165 shift/load port pin offset
#define PISOSHR_SH_LD		1
//! 74HC165 shift/load port pin data direction register
#define PISOSHR_SH_LD_DDR	DDRC
//! 74HC165 shift/load port pin data output register
#define PISOSHR_SH_LD_PORT	PORTC

//
// Key-Change-Interrupt
//

//! \brief Bit offset for key-change interrupt output port pin
#define KCI			3
//! \brief Data direction register for key-change interrupt output port pin
#define KCI_DDR			DDRC
//! \brief Data output register for key-change interrupt output pin
#define KCI_PORT		PORTC

//
// TWI
//

// Port pins PC5 and PC4 are used by the TWI bus interface

// PC5	SCL
// PC4	SDA

#endif // D_PINS_H
