//! \file sch.c
//! \brief Co-operative task scheduler

//
// Cooperative Task Scheduler
//

// This library provides an implementation of a cooperative task scheduler,
// entirely written in the C programming language, targeted for use in embedded systems.
//
// In order to use the scheduler, a hardware timer is required to cause periodic interrupts.
// A timer interupt will cause the scheduler to inspect its task list (by calling its Sch_Update()
// function) and check if a task is up to be run.
//
// The execution of tasks in then performed in a super loop, in the main execution context
// of the systems firmware (using the Sch_Dispatch_Tasks() function).
//
// The scheduler interface is designed to allow setting up tasks for execution,
// either periodic or single-shot (using the Sch_Add_Task() function).

// Sch_Init()		-- initialize scheduler
// Sch_Add_Task()	-- add task to scheduler task list
// Sch_Delete_Task()	-- delete task from task list
// Sch_Start()		-- enable the scheduler
// Sch_Update()		-- chech for tasks to be executed
// Sch_Dispatch_Tasks()	-- execute tasks
// Sch_Report()		-- get error/status code

// Example usage:
//
// #include "sch.h"
//
// Sch_Init();
//
// Sch_Add_Task(TaskFoo, 0,  500);
// Sch_Add_Task(TaskBar, 0, 1000);
// Sch_Add_Task(TaskBlub, 100, 0);
//
// Sch_Start();
//
// while(1)
// {
//	Sch_Dispatch_Tasks();
// }
//
// ISR()
// {
//	Sch_Update();
// }

///////////////////////////////////////////////////////////////////////////////
//
// Includes
//
///////////////////////////////////////////////////////////////////////////////

#include <inttypes.h>

#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "sch.h"
#include "timer.h"

///////////////////////////////////////////////////////////////////////////////
//
// Macro constants
//
///////////////////////////////////////////////////////////////////////////////

//! \brief Number of tasks that can be scheduled.
#define SCH_TASKS_MAX 4

//! \brief The "mode" in which the scheduler is to be used (see sch.h for more).
#define SCH_MODE		SCH_MODE_STANDARD

//--- For standard mode -------------------------------------------------------
//
// no further settings requried
//

//--- For SCI Master mode only ------------------------------------------------
//
// Configuration for which port pin to use for the shared clock signal output.
//

//! \brief Port pin offset for shared clock signal output.
#define SCH_CLK_OUT		7
//! \brief Data direction register for shared clock signal output.
#define SCH_CLK_OUT_DDR		DDRD
//! \brief Data output register for shared clock signal output.
#define SCH_CLK_OUT_PORT	PORTD

//--- For SCI Slave Mode only -------------------------------------------------
//
// Configuration for which port pin to use for the shared clock signal
// input (must be one of the external interrupt pins).
//

//! \brief Port pin offset for shared clock signal input.
#define SCH_CLK_IN		2
//! \brief Data direction register for shared clock signal input.
#define SCH_CLK_IN_DDR		DDRD

///////////////////////////////////////////////////////////////////////////////
//
// Macro functions
//
///////////////////////////////////////////////////////////////////////////////

//--- For SCI Master mode only ------------------------------------------------

//! \brief Configure port pin for shared clock output as output.
#define SCH_CLK_OUT_AS_OUTPUT()	(SCH_CLK_OUT_DDR |= (1 << SCH_CLK_OUT))

//! \brief Set output level for shared clock output to low.
#define SCH_CLK_OUT_SET_LOW()	(SCH_CLK_OUT_PORT &= ~(1 << SCH_CLK_OUT))
//! \brief Set output level for shared clock output to high.
#define SCH_CLK_OUT_SET_HIGH()	(SCH_CLK_OUT_PORT |= (1 << SCH_CLK_OUT))

//! \brief Toggle the output level of the shared clock output port pin.
#define SCH_CLK_OUT_TOGGLE()	(SCH_CLK_OUT_PORT & (1 << SCH_CLK_OUT) ? SCH_CLK_OUT_SET_LOW() : SCH_CLK_OUT_SET_HIGH())

//--- For SCI Slave mode only -------------------------------------------------

//! \brief Configure port pin for shared clock input as input.
#define SCH_CLK_IN_AS_INPUT()	(SCH_CLK_IN_DDR &= ~(1 << SCH_CLK_IN))

///////////////////////////////////////////////////////////////////////////////
//
// Custom data types
//
///////////////////////////////////////////////////////////////////////////////

//! \brief Task data structure data.
struct TaskStruct
{
	//! \brief Pointer to task function.
	void (* fp)(void);

	//! \brief Numer of ticks before the task is marked for execution.
	uint16_t delay;
	//! \brief Periode in ticks that specify the periode in which the task
	//! should be re-run.
	uint16_t periode;

	//! \brief Is set to non-zero when task is to be executed.
	uint8_t run;
};

//! \brief Task type.
typedef struct TaskStruct TaskStruct_t;

///////////////////////////////////////////////////////////////////////////////
//
// Private variables
//
///////////////////////////////////////////////////////////////////////////////

// List of tasks.
static TaskStruct_t s_task[SCH_TASKS_MAX];

///////////////////////////////////////////////////////////////////////////////
//
// Private functions
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Public functions
//
///////////////////////////////////////////////////////////////////////////////

//! \brief Initialize scheduler data structure and hardware timer.
//! \param tick_ms Time in miliseconds to be used as the tick interval.
//!
void Sch_Init(const uint16_t tick_ms)
{
	TaskId_t id;

	// Initialize task list with empty tasks.
	for(id = 0; id < SCH_TASKS_MAX; id++)
		Sch_Delete_Task(id);

	#if SCH_MODE == SCH_MODE_STANDARD || SCH_MODE == SCH_MODE_SCI_MASTER
	//
	// Setup for when using the scheduler in standard or "shared clock" master mode.
	//

	// Initialize timer1 device to tick every N miliseconds.
	Timer1_Init(tick_ms);
	#endif

	#if SCH_MODE == SCH_MODE_SCI_MASTER
	//
	// Setup for when using the scheduler in "shared clock" master mode.
	//

	// Setup clock pin as output.
	SCH_CLK_OUT_AS_OUTPUT();
	// Set output level to ...
	#endif

	#if SCH_MODE == SCH_MODE_SCI_SLAVE
	//
	// Setup for when using the scheduler in "shared clock" slave mode.
	//

	// Setup clock pin as input.
	SCH_CLK_IN_AS_INPUT();

	// Configure sense control for INT0 port pin.
	MCUCR |= (1 << ISC01); // "The falling edge of INT0 generates an interrupt request."

	// Enable INT0 interrupt.
	GICR |= (1 << INT0);
	#endif
}

//! \brief Add a task to the scheduler.
//! \param (*fp)() Pointer to task function to be scheduled.
//! \param delay Delay in ticks before the task is marked to run.
//! \param periode Periode in ticks when task is to be executed periodically.
//! \return The id of the added tasks or SCH_TASKS_MAX when adding the task failed.
//!
TaskId_t Sch_Add_Task(void (* fp)(void), const unsigned int delay, const unsigned int periode)
{
	TaskId_t id;

	id = 0;

	// Find the next free entry in the task list.
	while((s_task[id].fp != 0) && (id < SCH_TASKS_MAX))
		id++;

	// When id is equal to the max. number of task, this means
	// there is no free entry in the task list and the task
	// can not be added!
	if(id == SCH_TASKS_MAX)
	{
		return SCH_TASKS_MAX;
	}

	// Install task in task list.
	s_task[id].fp = fp;
	s_task[id].delay = delay;
	s_task[id].periode = periode;
	s_task[id].run = 0;

	return id;
}

//! \brief Delete a task.
//! \param id Numeric identifier for task to be deleted.
void Sch_Delete_Task(const TaskId_t id)
{
	s_task[id].fp = 0;
	s_task[id].delay = 0;
	s_task[id].periode = 0;
	s_task[id].run = 0;
}

//! \brief Start the scheduler.
//!
//! WHY DOES THIS NOT SHOW IN DOXYGEN?!
void Sch_Start(void)
{
	// Enable interrupts globally.
	sei();
}

// This function is to be called from the ISR.
static void Sch_Update(void)
{
	TaskId_t id;

	#if SCH_MODE == SCH_MODE_SCI_MASTER
	// Toggle clock output pin (only in SCI Master mode).
	SCH_CLK_OUT_TOGGLE();
	#endif

	for(id = 0; id < SCH_TASKS_MAX; id++)
	{
		// Check if there is valid entry (function pointer is not null).
		if(s_task[id].fp)
		{
			// Check if task delay is up for execution.
			if(s_task[id].delay == 0)
			{
				// Reset delay
				s_task[id].delay = s_task[id].periode;

				// Mark task for execution.
				s_task[id].run += 1;
			}
			else
			{
				// Task not ready to run. Decrement delay value by one.
				s_task[id].delay -= 1;
			}
		}
	}
}

//! \brief Execute tasks marked waiting.
//!
//! This function should be called from inside the super loop in the main() function.
void Sch_Dispatch_Tasks(void)
{
	TaskId_t id;

	for(id = 0; id < SCH_TASKS_MAX; id++)
	{
		if(s_task[id].run)
		{
			// Execute function.
			(*s_task[id].fp)();

			// Clear run flag.
			s_task[id].run -= 1;

			// When the task is a single-shot (has no periode) ...
			if(s_task[id].periode == 0)
			{
				// delete the task.
				Sch_Delete_Task(id);
			}
		}
	}

	// Report status.

	// Go to sleep.
	sleep_mode();
}


#if SCH_MODE == SCH_MODE_SCI_SLAVE

//! \brief Interrupt vector to use when using External Interrupt 0 port pin as input.
#define SCH_ISR_vect	INT0_vect

#else

//! \brief Interrupt vector to use when using on-chip Timer1.
#define SCH_ISR_vect	TIMER1_COMPA_vect

#endif

//! \brief Interrupt service routine that drives the update function.
ISR(SCH_ISR_vect)
{
	Sch_Update();
}
