//! \file kbd.c
//! \brief keyboard library
//!
//! The library manages the keypad hardware and checks for keys that have
//! changed their state. When a change is detected, a record is keep in a
//! buffer that allows to store multiple key events.
//!
//! To use the library, you will need to initialize it by calling Kbd_Init().
//! Then you need to make sure that the Kbd_Update() function will be called
//! periodically, and that the Kbd_GetKey() function is called to get the next
//! key event.
//!
//! This library is written for the lzoDSO keypad hardware. The keypad has
//! 41 keys in total. 25 push-buttons (normal open), 8 of them are part
//! of the 8 rotary encoders that are sources for CW or CCW keys (16 keys).

//--- Configuration -----------------------------------------------------------

//! Number of columns in the button matrix.
#define MATRIX_COLUMNS 5

//! Number of 74HC165 (PISO) shift-registers chained
#define PISOSHR_CHAINED 3

//! Size of the key event buffer in bytes.
#define KEYBUF_MAX 8

//! When SHR_SELECT_USING_SHIFT is defined, rather than writing the specific
//! address of a button matrix column just before every read, a "1" is written
//! on start of the scan to the 74HC164 (SIPO) shift register and is then
//! shifted on every read until overflows and starts from "1".
//#define SHR_SELECT_USING_SHIFT

//! Set DEBUG_SHR to enable writting the data read via
//! the 74HC165 shift register out to the UART for debugging.
//! \note This option assumes that the task that reads the keypad is scheduled
//! to run only once per second. Also, the size of the link buffer needs to be
//! large enough (29 bytes per matrix column). See shrbuf_dump().
#define DEBUG_SHR

//--- Includes ----------------------------------------------------------------

#include "kbd.h"
#include "siposhr.h"
#include "pisoshr.h"
#include "btnmtx.h"
#include "rotenc.h"
#include "pc_0.h"

//--- Private variables -------------------------------------------------------

// This two dimensional array of bytes is used to store the bytes received from
// the 74HC165 (PISO) shift-register. The array will contain the bytes received
// for a complete scan of the keypad (and its connected rotary encoders).
static uint8_t s_shrbuf[MATRIX_COLUMNS][PISOSHR_CHAINED];

//
// Key Buffer
//
// This buffer is filled by the Kbd_Update() function and is read using
// the Kbd_GetKey() function.

static uint8_t s_keybuf[KEYBUF_MAX];

static uint8_t s_keybuf_waiting_index;

static uint8_t s_keybuf_read_index;

//--- Private function prototypes ---------------------------------------------

static void shrbuf_init(void);
#ifdef DEBUG_SHR
static void shrbuf_dump(void);
#endif

static uint8_t kbd_scan(uint8_t *key);

//--- Public functions --------------------------------------------------------

//! \brief Initialize keyboard library
void Kbd_Init(void)
{
	// Initialize shift register buffer
	shrbuf_init();

	// Reset indexes for key event buffer.
	s_keybuf_waiting_index = 0;
	s_keybuf_read_index = 0;

	// Setup pins for 74HC164
	SIPOSHR_Init();
	// Setup pins for 74HC165
	PISOSHR_Init();

	// Initialize button matrix (software only)
	BtnMtx_Init();
	// Initialize rotary encoder (software only)
	RotEnc_Init();
}

//! \brief Will read (scan) the keypad and put the key read into a internal
//! buffer for later use.
void Kbd_Update(void)
{
	uint8_t key;

	if(kbd_scan(&key) == 0)
	{
		return;
	}

	if(s_keybuf_read_index == s_keybuf_waiting_index)
	{
		s_keybuf_waiting_index = 0;
		s_keybuf_read_index = 0;
	}

	s_keybuf[s_keybuf_waiting_index] = key;

	if(s_keybuf_waiting_index < KEYBUF_MAX)
	{
		s_keybuf_waiting_index++;
	}

#ifdef DEBUG_SHR
	// Dump the bytes, read from the shift register, during the scan of the keypad.
	shrbuf_dump();
#endif
}

//! \brief Get key event from keyboard event buffer
//! \param *key Pointer to where to write the key into.
//! \return Will return non-zero for valid keys, zero otherwise.
uint8_t Kbd_GetKey(uint8_t *key)
{
	if(s_keybuf_read_index < s_keybuf_waiting_index)
	{
		*key = s_keybuf[s_keybuf_read_index];

		s_keybuf_read_index++;

		return 1;
	}

	return 0;
}

//--- Private functions -------------------------------------------------------

#ifdef DEBUG_SHR
static void itob(uint8_t b, char *str)
{
	uint8_t i;
	char c;

	for(i = 0; i < 8; i++)
	{
		// Make sure to start with the MSB (left side), and work
		// down to the LSB (on the right side).
		c = (b & (1 << (7 - i))) ? '1' : '0';

		*(str+i) = c;
	}
}
#endif

static void shrbuf_init(void)
{
	uint8_t i;
	uint8_t k;

	for(i = 0; i < MATRIX_COLUMNS; i++)
	{
		for(k = 0; k < PISOSHR_CHAINED; k++)
		{
			s_shrbuf[i][k] = 0;
		}
	}
}

#ifdef DEBUG_SHR
static void shrbuf_dump(void)
{
	uint8_t i;
	char line[29]; // 1 + 1 + 8 + 1 + 8 + 1 + 8 + 1

	for(i = 0; i < MATRIX_COLUMNS; i++)
	{
		//
		// Prepare output
		//

		line[0] = '1'+i;
		line[1] = ' ';
		itob(s_shrbuf[i][0], line+2);
		// 3, 4, 5, 6, 7, 8, 9
		line[10] = ' ';
		itob(s_shrbuf[i][1], line+11);
		// 12, 13, 14, 15, 16, 17, 18
		line[19] = ' ';
		itob(s_shrbuf[i][2], line+20);
		// 21, 22, 23, 24, 25, 26,  27
		line[28] = '\0';

		//
		// Write output
		//

		link_Puts(line);
		link_Puts("\r\n");
	}
	link_Puts("\r\n");
}
#endif

static uint8_t kbd_scan(uint8_t *key)
{
	// Return value (0 by default)
	uint8_t rv = 0;
	uint8_t i;
	// receive buffer for the bytes read from the 74HC165 shift registers.
	uint8_t rxb[PISOSHR_CHAINED];

#ifdef SHR_SELECT_USING_SHIFT
	SIPOSHR_Write((1 << 0));
#endif

	// Scan the complete keypad.
	for(i = 0; i < MATRIX_COLUMNS; i++)
	{
		//
		// Select Address
		//

#if !defined(SHR_SELECT_USING_SHIFT)
		// Select column in matrix by writing a byte
		// where only a single bit is set.
		SIPOSHR_Write((1 << (7 - i)));
#endif

		//
		// Read from shift registers
		//

		// Make the 75HC165 shift registers latch/load the logic levels
		// of its parallel inputs, so they can be read in the next step.
		PISOSHR_Load();

		// Read the byte that contains the outputs of the button matrix
		rxb[0] = PISOSHR_Read();
		// Read the two bytes that contain the output levels of the rotary encoders.
		rxb[1] = PISOSHR_Read();
		rxb[2] = PISOSHR_Read();

		//
		// Process data
		//

		// Feed the button matrix column read into
		// the button matrix module.
		BtnMtx_SetColData(i, rxb[0]);

		// Feed the rotary encoder A/B pair data sets
		// into the rotary encoder library.
		RotEnc_SetData(0, rxb[1]);
		RotEnc_SetData(1, rxb[2]);

		//
		// Process button matrix keys
		//

		// Copy the bytes just read into keyboard buffer.
		if(BtnMtx_ColDataChanged(i))
		{
			rv++;

			BtnMtx_GetKey(i, key);
		}

		//
		// Process Rotary Encoder data
		//

		// NOTE: Since the rotary encoders are read on every
		// read of the button matrix column, the values of the encoders
		// need to be compared with the values from the read
		// before this one.

		if(RotEnc_DataChanged(0))
		{
			rv++;

			RotEnc_GetKey(key);
		}

		if(RotEnc_DataChanged(1))
		{
			rv++;

			RotEnc_GetKey(key);
		}

#ifdef SHR_SELECT_USING_SHIFT
		SIPOSHR_Shift();
#endif
	}

	return rv;
}
