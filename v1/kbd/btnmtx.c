//! \file btnmtx.c
//! \brief button matrix library

#include "btnmtx.h"

#define BTNMTX_COLS 5

static uint8_t s_keys_last[BTNMTX_COLS];
static uint8_t s_keys[BTNMTX_COLS];
static uint8_t s_debounce[BTNMTX_COLS];

//! \brief Initialize button matrix library
void BtnMtx_Init(void)
{
	uint8_t i;

	for(i = 0; i < BTNMTX_COLS; i++)
	{
		s_keys_last[i] = 0;
		s_keys[i] = 0;
		s_debounce[i] = 0;
	}
}

//! Insert column data into the logic of this library.
//! \param col Numeric identifier for the column the data belongs to
//! \param data Column data to feed into the library.
void BtnMtx_SetColData(const uint8_t col, const uint8_t data)
{
	if(col >= BTNMTX_COLS)
		return;

	s_keys[col] = data;
}

uint8_t BtnMtx_ColDataChanged(const uint8_t col)
{
	// Check if last and current column data differ.
	if(s_keys_last[col] != s_keys[col])
		return 1;

	return 0;
}

uint8_t BtnMtx_GetKey(const uint8_t col, uint8_t *key)
{
	uint8_t rv;
	uint8_t b;

	rv = 0;

	// Identify the key (bit) that changed
	for(b = 0; b < 8; b++)
	{
		if((s_keys_last[col] & (1 << (7 - b))) != (s_keys[col] & (1 << (7 - b))))
		{
			// Calculate and set the key id
			*key = (1 + col) + (BTNMTX_COLS * b);

			rv++;
		}
	}

	if(rv > 0)
	{
		s_keys_last[col] = s_keys[col];
	}

	return rv;
}
