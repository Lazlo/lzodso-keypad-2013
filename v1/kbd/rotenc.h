//! \file rotenc.h
//! \brief rotary encoder library

#ifndef D_ROTENC_H
#define D_ROTENC_H

#include <inttypes.h>

void RotEnc_Init(void);

void RotEnc_SetData(const uint8_t i, const uint8_t data);

uint8_t RotEnc_DataChanged(const uint8_t i);

uint8_t RotEnc_GetKey(uint8_t *key);

#endif // D_ROTENC_H
