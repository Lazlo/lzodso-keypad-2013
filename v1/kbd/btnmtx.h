//! \file btnmtx.h
//! \brief button matrix library

#ifndef D_BTNMTX_H
#define D_BTNMTX_H

#include <inttypes.h>

void BtnMtx_Init(void);
void BtnMtx_SetColData(const uint8_t col, const uint8_t data);
uint8_t BtnMtx_ColDataChanged(const uint8_t col);
uint8_t BtnMtx_GetKey(const uint8_t col, uint8_t *key);

#endif // D_BTNMTX_H
