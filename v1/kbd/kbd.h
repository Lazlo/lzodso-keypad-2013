//! \file kbd.h
//! \brief keyboard library

#ifndef D_KBD_H
#define D_KBD_H

#include <inttypes.h>

void Kbd_Init(void);

void Kbd_Update(void);

uint8_t Kbd_GetKey(uint8_t *key);

#endif // D_KBD_H
