//! \file rotenc.c
//! \brief rotary encoder library

#include "rotenc.h"

#define ROTENC_CONNECTED 8

static uint8_t s_state[2];
static uint8_t s_state_last[2];

//! \brief Initialize rotary encoder library
void RotEnc_Init(void)
{
	uint8_t i;

	for(i = 0; i < 2; i++)
	{
		s_state[i] = 0;
		s_state_last[i] = 0;
	}
}

//! \brief Feed a set of AB bit pairs into the library
//! \param i Address where to store the data
//! \param data Set of four A/B bit pairs
void RotEnc_SetData(const uint8_t i, const uint8_t data)
{
	s_state[i] = data;
}

uint8_t RotEnc_DataChanged(const uint8_t i)
{
	if(s_state_last[i] != s_state[i])
		return 1;

	return 0;
}

uint8_t RotEnc_GetKey(uint8_t *key)
{
	uint8_t rv;
	uint8_t i;
	uint8_t b;

	rv = 0;

	for(i = 0; i < 2; i++)
	{
		for(b = 0; b < 8; b++)
		{
			if((s_state_last[i] & (1 << (7 - b))) != (s_state[i] & (1 << (7 - b))))
			{
				*key = 25 + (i * 8) + (1 + b);

				rv++;
			}
		}
	}

	if(rv > 0)
	{
		s_state_last[0] = s_state[0];
		s_state_last[1] = s_state[1];
	}

	return rv;
}
