//! \file kci.h
//! \brief Key-Change Interrupt pin

#ifndef D_KCI_H
#define D_KCI_H

#include "pins.h"

//! \brief Configure KCI pin as output
#define KCI_AS_OUTPUT()	(KCI_DDR |= (1 << KCI))
//! \brief Make KCI pin output high
#define KCI_OUT_HIGH()	(KCI_PORT |= (1 << KCI))
//! \brief Make KCI pin output low
#define KCI_OUT_LOW()	(KCI_PORT &= ~(1 << KCI))

#endif // D_KCI_H
