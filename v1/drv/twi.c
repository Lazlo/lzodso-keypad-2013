//! \file twi.c
//! \brief Driver for the on-chip TWI interface

//--- Includes ----------------------------------------------------------------

#include "twi.h"

#include <avr/io.h>
#include <util/twi.h> // for TW_STATUS

//--- Private macros ----------------------------------------------------------

//! Check if hardware has complete last action (by checking if the TWINT bit in
//! the TWCR is set).
#define TWI_READY()		(TWCR & (1 << TWINT))

//
// Slave
//

//! Set the slave address.
#define TWI_SET_SLA(sla)	(TWAR = (sla << 1))

//! Enable addressing using general calls.
#define TWI_SET_GCE()		(TWAR |= (1 << TWGCE))

//! Enable the TWI device (and slave address).
#define TWI_ST_INIT_ENABLE()	(TWCR = (1 << TWEA)|(1 << TWEN))

#define TWI_ST_WRITE_ACK()	(TWCR = (1 << TWINT)|(1 << TWEA)|(1 << TWEN))
#define TWI_ST_WRITE_NACK()	(TWCR = (1 << TWINT)|(1 << TWEN))
#define TWI_ST_RESET()		(TWCR = (1 << TWINT)|(1 << TWEA)|(1 << TWEN))

//--- Private functions -------------------------------------------------------

//
// Common (used in TWI modes)
//

static uint8_t wait_with_timeout(uint16_t timeout)
{
	// Wait for the action to complete (or timeout).
	while(!TWI_READY() && timeout--)
		;

	// Check if timeout has cause loop to stop.
	if(timeout == 0)
	{
		// Return failure due to timeout.
		return 1;
	}

	// Return success.
	return 0;
}

//--- Public functions --------------------------------------------------------

//
// Common
//

// Call this function to check if there is a new status code to be read.
//
// Return non-zero when the hardware has complete its last operation and a new
// status code is ready to be read. While hardware is busy, returns 0.
uint8_t TWI_StatusChanged(void)
{
	// Check if the TWINT bit in the TWCR is set.
	return TWI_READY();
}

uint8_t TWI_GetStatus(void)
{
	return TW_STATUS;
}

//
// Slave
//

//! Initialize TWI as slave.
//! \param sla Slave address.
void TWI_Init(SLA_t sla, const uint8_t gce)
{
	// Set the slave address.
	TWI_SET_SLA(sla);

	// Set general call enable bit (when gce is non-zero).
	if(gce)
		TWI_SET_GCE();

	// Enable the device.
	TWI_ST_INIT_ENABLE();
}

//! \brief Send a byte as Slave Transmitter
//! \param data Byte to transmitt.
//! \return Returns 0 on success, 1 on timeout, status code for all other failed cases.
uint8_t TWI_Write(const uint8_t byte, const uint8_t ack)
{
	uint16_t timeout = 1000;
	uint8_t status;
	// Expected status (for Slave Transmitter Mode).
	uint8_t status_expected;


	//
	// Prepare data to be send
	//

	// Load byte into data register.
	TWDR = byte;

	//
	// Transmitt the data
	//

	// Enable data byte.
	if(ack)
	{
		TWI_ST_WRITE_ACK();
		// "Data byte in TWDR has been transmitted; ACK has been received"
		status_expected = 0xB8;
	}
	else
	{
		TWI_ST_WRITE_NACK();
		// "Last data byte in TWDR has been transmitted (TWEA = 0); ACK has been received"
		status_expected = 0xC8;
	}

	//
	// Wait for completion
	//

	if(wait_with_timeout(timeout))
	{
		return 1;
	}

	//
	// Verify outcome
	//

	// Get status.
	status = TW_STATUS;

	// Verify status.
	if(status == status_expected)
	{
		// Return success.
		return 0;
	}

	// Return failure (status).
	return status;
}

void TWI_ST_Reset(void)
{
	TWI_ST_RESET();
}
