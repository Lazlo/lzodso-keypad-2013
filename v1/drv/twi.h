//! \file twi.h
//! \brief Driver for the on-chip TWI interface

#ifndef D_TWI_H
#define D_TWI_H

//--- Includes ----------------------------------------------------------------

#include <inttypes.h>

//--- Custom types ------------------------------------------------------------

//! \brief Type for slave address.
typedef uint8_t SLA_t;

//--- Public functions --------------------------------------------------------

//
// Common
//

uint8_t TWI_StatusChanged(void);

uint8_t TWI_GetStatus(void);

//
// Slave
//

void TWI_Init(SLA_t sla, const uint8_t gce);

//
// Slave Transmitter
//

uint8_t TWI_Write(const uint8_t byte, const uint8_t ack);

void TWI_ST_Reset(void);

#endif // D_TWI_H
