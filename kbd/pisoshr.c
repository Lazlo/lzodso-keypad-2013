#include "pisoshr.h"
#include <util/delay.h>
#include <avr/io.h>

#define PISOSHR_CLK		(3)
#define PISOSHR_CLK_DDR		DDRC
#define PISOSHR_CLK_PORT	PORTC

#define PISOSHR_Q		(5)
#define PISOSHR_Q_DDR		DDRC
#define PISOSHR_Q_PIN		PINC

#define PISOSHR_SH_LD		(4)
#define PISOSHR_SH_LD_DDR	DDRC
#define PISOSHR_SH_LD_PORT	PORTC

//--- Timing related macro constants ------------------------------------------

#define PISOSHR_DELAY_LOAD_US	10

#define PISOSHR_DELAY_CLOCK_US	10

//--- Private macro functions -------------------------------------------------

//
// Macros for setting port pin directions
//

#define PISOSHR_CLK_AS_OUTPUT()		(PISOSHR_CLK_DDR |= (1 << PISOSHR_CLK))

#define PISOSHR_Q_AS_INPUT()		(PISOSHR_Q_DDR &= ~(1 << PISOSHR_Q))

#define PISOSHR_SH_LD_AS_OUTPUT()	(PISOSHR_SH_LD_DDR |= (1 << PISOSHR_SH_LD))

//
// Macros for setting port pin output levels
//

#define PISOSHR_CLK_OUT_HIGH()		(PISOSHR_CLK_PORT |= (1 << PISOSHR_CLK))
#define PISOSHR_CLK_OUT_LOW()		(PISOSHR_CLK_PORT &= ~(1 << PISOSHR_CLK))

#define PISOSHR_SH_LD_OUT_HIGH()	(PISOSHR_SH_LD_PORT |= (1 << PISOSHR_SH_LD))
#define PISOSHR_SH_LD_OUT_LOW()		(PISOSHR_SH_LD_PORT &= ~(1 << PISOSHR_SH_LD))

//
// Macros for reading port pins
//

#define PISOSHR_Q_READ()		(PISOSHR_Q_PIN & (1 << PISOSHR_Q))

//--- Public functions --------------------------------------------------------

void PISOSHR_Init(void)
{
	//
	// Set port pin directions
	//

	// Setup clock pin as output.
	PISOSHR_CLK_AS_OUTPUT();

	// Setup data (Q) pin as input.
	PISOSHR_Q_AS_INPUT();

	// Setup shift/load pin as output.
	PISOSHR_SH_LD_AS_OUTPUT();

	//
	// Set port pin output levels
	//

	// Set clock output to low.
	PISOSHR_CLK_OUT_LOW();

	// Set shift/load output to high (shift).
	PISOSHR_SH_LD_OUT_HIGH();
}

//! Will cause the shift register to latch the values of the parallel inputs.
//! Must call this function before shifting bits out to read them.
void PISOSHR_Load(void)
{
	// Pull shift/load port pin to low (load).
	PISOSHR_SH_LD_OUT_LOW();

	_delay_us(PISOSHR_DELAY_LOAD_US);

	// Restore shift/load port pin to high (shift).
	PISOSHR_SH_LD_OUT_HIGH();
}

//! Read a byte from the shift register.
//! \return Byte read from the shift register.
uint8_t PISOSHR_Read(void)
{
	uint8_t rv = 0;
	uint8_t i;

	for(i = 0; i < 8; i++)
	{
		if(PISOSHR_Q_READ())
		{
			rv |= (1 << i);
		}

		// Advance clock (low-to-high)
		PISOSHR_CLK_OUT_HIGH();

		_delay_us(PISOSHR_DELAY_CLOCK_US);

		// Restore clock at low
		PISOSHR_CLK_OUT_LOW();
	}

	return rv;
}
