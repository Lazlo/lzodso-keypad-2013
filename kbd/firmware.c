//! \file firmware.c
//
// Keyboard controller firmware.
//
// Will periodically scan the keypad for changes.
//
// This firmware is written for a ATmega8, clocked using a 16 MHz quarz crystal.

#include "pins.h"
#include "uart.h"
#include "sch.h"
#include "kbd.h"
#include "strutils.h"

static void Read_Kbd_Task(void);

int main(void)
{
	Sch_Init(100);

	UART_Init(38400);

	UART_Puts("\r\n");
	UART_Puts("uart ready!\r\n");

	Kbd_Init();
	UART_Puts("kbd ready!\r\n");

	// Execute 'read keyboard' task every 100 miliseconds.
	Sch_Add_Task(Read_Kbd_Task, 0, 1);

	Sch_Start();
	UART_Puts("sched ready!\r\n");

	while(1)
	{
		Sch_Dispatch_Tasks();
	}

	return 0;
}

//!
//!
static void Read_Kbd_Task(void)
{
	char key;
	char keystr[3];

	if(Kbd_Read(&key))
	{
		itoa(key, keystr, 10);

		UART_Puts(keystr);
		UART_Puts("\r\n");
	}
}
