#include "pins.h"
#include "kbd.h"

//--- Private macro functions -------------------------------------------------

//
// Macros for setting port pin directions.
//

#define SIPOSHR_CP_AS_OUTPUT()		(SIPOSHR_CP_DDR |= (1 << SIPOSHR_CP))
#define SIPOSHR_DS1_AS_OUTPUT()		(SIPOSHR_DS1_DDR |= (1 << SIPOSHR_DS1))
#define SIPOSHR_MR_AS_OUTPUT()		(SIPOSHR_MR_DDR |= (1 << SIPOSHR_MR))

#define PISOSHR_CLK_AS_OUTPUT()		(PISOSHR_CLK_DDR |= (1 << PISOSHR_CLK))
#define PISOSHR_Q_AS_INPUT()		(PISOSHR_Q_DDR &= ~(1 << PISOSHR_Q))
#define PISOSHR_SH_LD_AS_OUTPUT()	(PISOSHR_SH_LD_DDR |= (1 << PISOSHR_SH_LD))

//
// Macros for setting port in output levels.
//

#define SIPOSHR_CP_OUT_HIGH()		(SIPOSHR_CP_PORT |= (1 << SIPOSHR_CP))
#define SIPOSHR_CP_OUT_LOW()		(SIPOSHR_CP_PORT &= ~(1 << SIPOSHR_CP))

#define SIPOSHR_DS1_OUT_HIGH()		(SIPOSHR_DS1_PORT |= (1 << SIPOSHR_DS1))
#define SIPOSHR_DS1_OUT_LOW()		(SIPOSHR_DS1_PORT &= ~(1 << SIPOSHR_DS1))

#define SIPOSHR_MR_OUT_HIGH()		(SIPOSHR_MR_PORT |= (1 << SIPOSHR_MR))
#define SIPOSHR_MR_OUT_LOW()		(SIPOSHR_MR_PORT &= ~(1 << SIPOSHR_MR))

#define PISOSHR_CLK_OUT_HIGH()		(PISOSHR_CLK_PORT |= (1 << PISOSHR_CLK))
#define PISOSHR_CLK_OUT_LOW()		(PISOSHR_CLK_PORT &= ~(1 << PISOSHR_CLK))

#define PISOSHR_SH_LD_OUT_HIGH()	(PISOSHR_SH_LD_PORT |= (1 << PISOSHR_SH_LD))
#define PISOSHR_SH_LD_OUT_LOW()		(PISOSHR_SH_LD_PORT &= ~(1 << PISOSHR_SH_LD))

#define PISOSHR_Q_IN_READ()		(PISOSHR_Q_PIN & (1 << PISOSHR_Q))

//--- Private function prototypes ---------------------------------------------

static void siposhr_init(void);
static void siposhr_reset(void);
static void siposhr_write(char c);
static void siposhr_shift(void);

static void pisoshr_init(void);
static void pisoshr_load(void);
static char pisoshr_read(void);

static void keypad_scan(void);

//--- Private variables -------------------------------------------------------

static uint8_t s_mtxbuf[5];

//--- Public functions --------------------------------------------------------

void Kbd_Init(void)
{
	uint8_t i;

	// Reset the matrix buffer.
	for(i = 0; i < 5; i++)
		s_mtxbuf[i] = 0;

	// Setup pins for 74HC164 (SIPO) shift register.
	siposhr_init();

	// Setup pins for 74HC165 (PISO) shift register.
	pisoshr_init();
}

char Kbd_Read(char *key)
{
	char rv = 0;

	keypad_scan();

	// Check for changes
	return rv;
}

//--- Private functions -------------------------------------------------------

static void keypad_scan(void)
{
	uint8_t i;
	uint8_t k;

	uint8_t inbuf[3];

	// Reset address selection
	siposhr_reset();

	// Select initial address in matrix
	siposhr_write(1);

	// For each address in the matrix ...
	for(i = 0; i < 5; i++)
	{
		// Latch row output values in PISO shift register.
		pisoshr_load();

		// Read 3 bytes from PISO shift register.
		for(k = 0; k < 3; k++)
		{
			inbuf[k] = pisoshr_read();
		}

		// Copy the content of the first byte from the local inbuf
		// into the matrix buffer.
		if(s_mtxbuf != inbuf[0])
		{
			s_mtxbuf[i] = inbuf[0];
		}

		// Select next address in matrix
		siposhr_shift();
	}
}

//
// Functions for the 74HC164 (SIPO) shift register
//

static void siposhr_init(void)
{
	// Set port pin directions.
	SIPOSHR_CP_AS_OUTPUT();
	SIPOSHR_DS1_AS_OUTPUT();
	SIPOSHR_MR_AS_OUTPUT();

	// Set output levels.
	SIPOSHR_CP_OUT_LOW();
	SIPOSHR_DS1_OUT_LOW();
	SIPOSHR_MR_OUT_HIGH();
}

static void siposhr_reset(void)
{
	// Reset is active-low

	// Pull the reset line low.
	SIPOSHR_MR_OUT_LOW();

	// Restore reset line to high.
	SIPOSHR_MR_OUT_HIGH();
}

static void siposhr_write(char c)
{
	uint8_t i;

	// Data is shifted on the positive edge of the clock.
	// This means a low-to-high transistion is needed to
	// advance the clock.

	for(i = 0; i < 8; i++)
	{
		// Restore clock
		SIPOSHR_CP_OUT_LOW();

		if(c & (1 << (7 - i)))	SIPOSHR_DS1_OUT_HIGH();
		else			SIPOSHR_DS1_OUT_LOW();

		// Advance clock
		SIPOSHR_CP_OUT_HIGH();

		// Possibly wait (hold) the clock high for ...FIXME...
	}
}

static void siposhr_shift(void)
{
	// Restore clock
	SIPOSHR_CP_OUT_LOW();

	// Advance clock
	SIPOSHR_CP_OUT_HIGH();

	// Possibly wait (hold) the clock high for ...FIXME...
}

//
// Functions for the 74HC165 (PISO) shift register
//

static void pisoshr_init(void)
{
	// Set port pin directions.
	PISOSHR_CLK_AS_OUTPUT();
	PISOSHR_Q_AS_INPUT();
	PISOSHR_SH_LD_AS_OUTPUT();

	// Set output levels.
	PISOSHR_CLK_OUT_LOW();
	PISOSHR_SH_LD_OUT_HIGH();
}

static void pisoshr_load(void)
{
	// Pull low for "load"
	PISOSHR_SH_LD_OUT_LOW();

	// Restore to "shift"
	PISOSHR_SH_LD_OUT_HIGH();
}

static char pisoshr_read_bit(void)
{
	// Restore clock
	PISOSHR_CLK_OUT_LOW();

	// Advance clock
	PISOSHR_CLK_OUT_HIGH();

	// Possibly wait (hold the clock high for ...FIXME...

	return PISOSHR_Q_IN_READ();
}

static char pisoshr_read(void)
{
	uint8_t i;
	uint8_t rv;

	rv = 0;

	for(i = 0; i < 8; i++)
	{
		if(pisoshr_read_bit())
			rv |= (1 << i);
	}

	return rv;
}
