#ifndef D_PINS_H
#define D_PINS_H

#include <avr/io.h>

//--- 74HC164 (SIPO) port pins ------------------------------------------------

//! clock output pin
#define SIPOSHR_CP		0
#define SIPOSHR_CP_DDR		DDRC
#define SIPOSHR_CP_PORT		PORTC

//! data output pin
#define SIPOSHR_DS1		2
#define SIPOSHR_DS1_DDR		DDRC
#define SIPOSHR_DS1_PORT	PORTC

//! master reset output pin
#define SIPOSHR_MR		1
#define SIPOSHR_MR_DDR		DDRC
#define SIPOSHR_MR_PORT		PORTC

//--- 74HC165 (PISO) port pins ------------------------------------------------

//! clock output pin
#define PISOSHR_CLK		3
#define PISOSHR_CLK_DDR		DDRC
#define PISOSHR_CLK_PORT	PORTC

//! data input pin
#define PISOSHR_Q		5
#define PISOSHR_Q_DDR		DDRC
#define PISOSHR_Q_PIN		PINC

//! shift/load output pin
#define PISOSHR_SH_LD		4
#define PISOSHR_SH_LD_DDR	DDRC
#define PISOSHR_SH_LD_PORT	PORTC

#endif // D_PINS_H
