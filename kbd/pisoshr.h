#ifndef D_PISOSHR_H
#define D_PISOSHR_H

#include <inttypes.h>

void PISOSHR_Init(void);

void PISOSHR_Load(void);

uint8_t PISOSHR_Read(void);

#endif // D_PISOSHR_H
