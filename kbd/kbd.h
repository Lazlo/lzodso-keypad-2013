#ifndef D_KBD_H
#define D_KBD_H

void Kbd_Init(void);

char Kbd_Read(char *key);

#endif // D_KBD_H
