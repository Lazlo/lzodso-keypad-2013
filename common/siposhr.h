//! \file siposhr.h
//! \brief 74HC164 (SIPO) shift register driver

#ifndef D_SIPOSHR_H
#define D_SIPOSHR_H

#include <inttypes.h>

void SIPOSHR_Init(void);

void SIPOSHR_Reset(void);

void SIPOSHR_Shift(void);

void SIPOSHR_Write(uint8_t b);

#endif // D_SIPOSHR_H
