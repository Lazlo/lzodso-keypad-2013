//! \file pisoshr.c
//! \brief 74HC165 (PISO) shift register driver

#include "pisoshr.h"
#include <util/delay.h>
#include <avr/io.h>

//! 74HC165 clock port pin offset
//#define PISOSHR_CLK		(0)
//! 74HC165 clock port pin data direction register
//#define PISOSHR_CLK_DDR		DDRC
//! 74HC165 clock port pin data output register
//#define PISOSHR_CLK_PORT	PORTC

//! 74HC165 data port pin offset
//#define PISOSHR_Q		(2)
//! 74HC165 data port pin data direction register
//#define PISOSHR_Q_DDR		DDRC
//! 74HC165 data port pin data input register
//#define PISOSHR_Q_PIN		PINC

//! 74HC165 shift/load port pin offset
//#define PISOSHR_SH_LD		(1)
//! 74HC165 shift/load port pin data direction register
//#define PISOSHR_SH_LD_DDR	DDRC
//! 74HC165 shift/load port pin data output register
//#define PISOSHR_SH_LD_PORT	PORTC

#include "pins.h"

//--- Timing related macro constants ------------------------------------------

#define PISOSHR_DELAY_LOAD_US	1

#define PISOSHR_DELAY_CLOCK_US	1

//--- Private macro functions -------------------------------------------------

//
// Macros for setting port pin directions
//

//! Set port pin data direction for clock pin to output
#define PISOSHR_CLK_AS_OUTPUT()		(PISOSHR_CLK_DDR |= (1 << PISOSHR_CLK))
//! Set port pin data direction for data pin to input
#define PISOSHR_Q_AS_INPUT()		(PISOSHR_Q_DDR &= ~(1 << PISOSHR_Q))
//! Set port pin data direction for shift/load pin to output
#define PISOSHR_SH_LD_AS_OUTPUT()	(PISOSHR_SH_LD_DDR |= (1 << PISOSHR_SH_LD))

//
// Macros for setting port pin output levels
//

//! Make clock port pin output high
#define PISOSHR_CLK_OUT_HIGH()		(PISOSHR_CLK_PORT |= (1 << PISOSHR_CLK))
//! Make clock port pin output low
#define PISOSHR_CLK_OUT_LOW()		(PISOSHR_CLK_PORT &= ~(1 << PISOSHR_CLK))

//! Make shift/load port pin output high
#define PISOSHR_SH_LD_OUT_HIGH()	(PISOSHR_SH_LD_PORT |= (1 << PISOSHR_SH_LD))
//! Make shift/load port pin output low
#define PISOSHR_SH_LD_OUT_LOW()		(PISOSHR_SH_LD_PORT &= ~(1 << PISOSHR_SH_LD))

//
// Macros for reading port pins
//

//! Read bit from data input port pin
#define PISOSHR_Q_READ()		(PISOSHR_Q_PIN & (1 << PISOSHR_Q))

//--- Public functions --------------------------------------------------------

//! \brief Initialize 74HC165 port pins.
void PISOSHR_Init(void)
{
	//
	// Set port pin directions
	//

	// Setup clock pin as output.
	PISOSHR_CLK_AS_OUTPUT();

	// Setup data (Q) pin as input.
	PISOSHR_Q_AS_INPUT();

	// Setup shift/load pin as output.
	PISOSHR_SH_LD_AS_OUTPUT();

	//
	// Set port pin output levels
	//

	// Set clock output to low.
	PISOSHR_CLK_OUT_LOW();

	// Set shift/load output to high (shift).
	PISOSHR_SH_LD_OUT_HIGH();
}

//! Will cause the shift register to latch the values of the parallel inputs.
//! Must call this function before shifting bits out to read them.
void PISOSHR_Load(void)
{
	// Pull shift/load port pin to low (load).
	PISOSHR_SH_LD_OUT_LOW();

	_delay_us(PISOSHR_DELAY_LOAD_US);

	// Restore shift/load port pin to high (shift).
	PISOSHR_SH_LD_OUT_HIGH();
}

//! Read a byte from the shift register.
//! \return Byte read from the shift register.
uint8_t PISOSHR_Read(void)
{
	uint8_t rv = 0;
	uint8_t i;

	for(i = 0; i < 8; i++)
	{
		if(PISOSHR_Q_READ())
		{
			rv |= (1 << i);
		}

		// Advance clock (low-to-high)
		PISOSHR_CLK_OUT_HIGH();

		_delay_us(PISOSHR_DELAY_CLOCK_US);

		// Restore clock at low
		PISOSHR_CLK_OUT_LOW();
	}

	return rv;
}
