//! \file siposhr.c
//! \brief 74HC164 (SIPO) shift register driver

#include "siposhr.h"
#include <avr/io.h>
#include <util/delay.h>

//--- Port pin configuration macros -------------------------------------------

//! 74HC164 clock port pin offset
//#define SIPOSHR_CP		5
//! 74HC164 clock port pin data direction register
//#define SIPOSHR_CP_DDR		DDRD
//! 74HC164 clock port pin data output register
//#define SIPOSHR_CP_PORT		PORTD

//! 74HC164 data port pin offset
//#define SIPOSHR_DS1		7
//! 74HC164 data port pin data direction register
//#define SIPOSHR_DS1_DDR		DDRD
//! 74HC164 data port pin data output register
//#define SIPOSHR_DS1_PORT	PORTD

//! 74HC164 reset port pin offset
//#define SIPOSHR_MR		6
//! 74HC164 reset port pin data direction register
//#define SIPOSHR_MR_DDR		DDRD
//! 74HC164 reset port pin data output register
//#define SIPOSHR_MR_PORT		PORTD

#include "pins.h"

//--- Timing related macro constants ------------------------------------------

#define SIPOSHR_DELAY_RESET_US	1

#define SIPOSHR_DELAY_SHIFT_US	1

#define SIPOSHR_DELAY_DATA_US	1

//--- Private macro functions for setting port pin directions -----------------

//! Set port pin direction for clock pin to output
#define SIPOSHR_CP_AS_OUTPUT()	(SIPOSHR_CP_DDR |= (1 << SIPOSHR_CP))

//! Set port pin direction for data pin to output
#define SIPOSHR_DS1_AS_OUTPUT()	(SIPOSHR_DS1_DDR |= (1 << SIPOSHR_DS1))

//! Set port pin direction for reset pin to output
#define SIPOSHR_MR_AS_OUTPUT()	(SIPOSHR_MR_DDR |= (1 << SIPOSHR_MR))

//--- Private macro function for setting port pin output levels ---------------

//! Make clock port pin output high
#define SIPOSHR_CP_OUT_HIGH()	(SIPOSHR_CP_PORT |= (1 << SIPOSHR_CP))
//! Make clock port pin output low
#define SIPOSHR_CP_OUT_LOW()	(SIPOSHR_CP_PORT &= ~(1 << SIPOSHR_CP))

//! Make data port pin output high
#define SIPOSHR_DS1_OUT_HIGH()	(SIPOSHR_DS1_PORT |= (1 << SIPOSHR_DS1))
//! Make data port pin output low
#define SIPOSHR_DS1_OUT_LOW()	(SIPOSHR_DS1_PORT &= ~(1 << SIPOSHR_DS1))

//! Make reset port pin output high
#define SIPOSHR_MR_OUT_HIGH()	(SIPOSHR_MR_PORT |= (1 << SIPOSHR_MR))
//! Make reset port pin output low
#define SIPOSHR_MR_OUT_LOW()	(SIPOSHR_MR_PORT &= ~(1 << SIPOSHR_MR))

//--- Public functions --------------------------------------------------------

//! \brief Initialize port pins for shift register.
void SIPOSHR_Init(void)
{
	//
	// Set port pin directions
	//

	SIPOSHR_CP_AS_OUTPUT();
	SIPOSHR_DS1_AS_OUTPUT();
	SIPOSHR_MR_AS_OUTPUT();

	//
	// Set port pin output levels
	//

	SIPOSHR_CP_OUT_LOW();
	SIPOSHR_DS1_OUT_LOW();
	SIPOSHR_MR_OUT_HIGH();
}

//! \brief Reset value in shift register.
void SIPOSHR_Reset(void)
{
	SIPOSHR_MR_OUT_LOW();

	_delay_us(SIPOSHR_DELAY_RESET_US);

	SIPOSHR_MR_OUT_HIGH();
}

//! Shift the bits in the shift register
void SIPOSHR_Shift(void)
{
	// Advance clock
	SIPOSHR_CP_OUT_HIGH();

	_delay_us(SIPOSHR_DELAY_SHIFT_US);

	// Restore clock
	SIPOSHR_CP_OUT_LOW();
}

//! Write byte to shift register
//! \param b Byte to be written
void SIPOSHR_Write(uint8_t b)
{
	uint8_t i;

	// Shift out LSB first.
	for(i = 0; i < 8; i++)
	{
		if(b & (1 << i))	SIPOSHR_DS1_OUT_HIGH();
		else			SIPOSHR_DS1_OUT_LOW();

		_delay_us(SIPOSHR_DELAY_DATA_US);

		SIPOSHR_Shift();
	}
}
